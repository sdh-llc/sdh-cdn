from django.apps import AppConfig


class SdhCdnApp(AppConfig):
    name = 'sdh.cdn'
    verbose_name = 'SDH CDN engine'
